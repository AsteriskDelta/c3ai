#include "C3AI.h"
using namespace C3AI;


int main(int argc, char** argv) {
    _unused(argc, argv);
    
    Symlink a, b, c, d, e;
    Symlink *endPtr = &e;
    
    Symlink *links = &a;
    
    //a,b,d intersect; c,d intersect;
    a.setRange(0, 0);
    b.setRange(2, 6);
    //a.set(5.28, sq(0.91));
    //b.set(8.45, sq(1.36));
    c.setRange(10, 1);
    d.setRange(5, 5);
    e.setRange(3, 1);
    
    for(Symlink *l = links; l <= endPtr; l++) {
        l->weight = 1;
        l->update();
    }
    
    a.weight = 3;
    b.weight = 0.8;
    
    for(Symlink *l = links; l <= endPtr; l++) {
        std::cout << "0x" << l << ": " << l->debugInfo() << "\n";
    }
    
    for(Symlink *x = links; x <= endPtr; x++) {
        for(Symlink *y = links; y <= endPtr; y++) {
            if(x == y) continue;
            
            bool inter = x->intersects(*y);
            if(!inter) {
                std::cout << "0x" << x << " misses " << " 0x" << y << "\n";
                continue;
            }
            
            std::cout << "0x" << x << " intersects " << " 0x" << y << "\n"; 
            std::cout << "\tIntersection: point=" << x->intersectionPoint(*y) << " =?= " << y->intersectionPoint(*x) <<" prob=" << x->intersectionProb(*y) << "  integral=" << x->intersection(*y) << "\n";
            std::cout << "\tContainment PFs: " << x->contains(*y) << " <-> " << y->contains(*x) << "\n";
            std::cout << "\tScore: " << x->score(*y)/* << " AvgScore: " << x->symmetricAvgScore(*y) << " Asym: " << x->asymmetricScore(*y) << " Diff: " << x->differential(*y) */<< " for u1=" << x->mean() << ", o1=" << x->stddev()<<", W1=" << x->weight << " u2=" << y->mean() << ", o2=" << y->stddev() << ", W2=" << y->weight << "\n";
        }
    }
    
    return 0;
}
