#ifndef C3AI_EXTERNALS
#define C3AI_EXTERNALS

#include <Spinster/Spinster.h>

#include <ARKE/ARKE.h>
#include <ARKE/Application.h>
#include <ARKE/Console.h>
namespace Application = arke::Application;
using arke::Console;


#include <NVX/NVX.h>
//#include <ARKE/EngineClient.h>
//#include <ARKE/UI.h>

#include <mutex>

//#include <TMSC/TMSCInclude.h>
//#include <TMSC/TMSCFundamental.h>
//#include <TMSC/render/DebugGroup.h>


#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <cstdint>

using byte=std::byte;
using ptr_t=std::ptrdiff_t;

#endif
