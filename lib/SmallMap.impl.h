#include "SmallMap.h"
#include <cstring>
#include <NVX/NGaussian.h>

#define SMALLMAP_TPL template<typename KEY, typename VALUE>
#define SMALLMAP_CLS SmallMap<KEY, VALUE>

static unsigned char SMP_InvalidKey[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};

SMALLMAP_TPL
bool SMALLMAP_CLS::Pair::valid() const {
    return memcmp(&key, SMP_InvalidKey, sizeof(Key)) != 0;
}
SMALLMAP_TPL
void SMALLMAP_CLS::Pair::invalidate() {
    //memset(&key, 0xFF, sizeof(Key));
}

SMALLMAP_TPL
thread_local typename SMALLMAP_CLS::Value SMALLMAP_CLS::nilValue = SMALLMAP_CLS::Value();

SMALLMAP_TPL
SMALLMAP_CLS::SmallMap() : Arith(this), values(), unused() {
    
}
SMALLMAP_TPL
SMALLMAP_CLS::SmallMap(const SmallMap& orig) : Arith(this),values(), unused() {
    values = orig.values;
    unused = orig.unused;
}
SMALLMAP_TPL
SMALLMAP_CLS::~SmallMap() {
    
}

SMALLMAP_TPL
inline typename SMALLMAP_CLS::Idt SMALLMAP_CLS::getID(typename  SMALLMAP_CLS::ConstPair *const& pair) const {
    return static_cast<Idt>(pair - &values[0]);
}

SMALLMAP_TPL
bool SMALLMAP_CLS::has(const typename SMALLMAP_CLS::Key& key) const noexcept {
    for(const Pair& pair : values) {
        if(pair.key == key) return true;
    }
    return false;
}
SMALLMAP_TPL
const typename SMALLMAP_CLS::Pair* SMALLMAP_CLS::get(const typename SMALLMAP_CLS::Key& key) const {
    for(const Pair& pair : values) {
        if(pair.key == key) return &pair;
    }
    return nullptr;
}
SMALLMAP_TPL
typename SMALLMAP_CLS::Pair* SMALLMAP_CLS::get(const typename SMALLMAP_CLS::Key& key, const bool& create) {
    for(Pair& pair : values) {
        if(pair.key == key) return &pair;
    }
    if(create) {
        return this->insert(key, Value());
    } else return nullptr;
}
SMALLMAP_TPL
const typename SMALLMAP_CLS::Value& SMALLMAP_CLS::at(const typename SMALLMAP_CLS::Key& key) const {
    const Pair *const pair = this->get(key);
    if(pair == nullptr) return nilValue;
    else return pair->value;
}
SMALLMAP_TPL
typename SMALLMAP_CLS::Value& SMALLMAP_CLS::at(const typename SMALLMAP_CLS::Key& key, const bool& create) {
    Pair *const pair = this->get(key, create);
    if(pair == nullptr) return nilValue;
    else return pair->value;
}

SMALLMAP_TPL
bool SMALLMAP_CLS::erase(const typename SMALLMAP_CLS::Key& key) {
    for(Pair& pair : values) {
        if(pair.key == key) {
            pair.invalidate();
            pair.value.~Value();
            unused.push_back(this->getID(&pair));
            return true;
        }
    }
    return false;
}
SMALLMAP_TPL
typename SMALLMAP_CLS::Pair* SMALLMAP_CLS::insert(const typename SMALLMAP_CLS::Key& key, const typename SMALLMAP_CLS::Value& val, const bool& force) {
    if(!force) {
        Pair *existingPair = this->get(key);
        if(existingPair != nullptr) return existingPair;
    }
    
    if(!unused.empty()) {
        Pair *const pair = &values[unused.back()];
        unused.pop_back();
        
        pair->key = key;
       new(&(pair->value)) Value(val);
       return pair;
    } else {
        values.push_back(Pair{key,val});
        return &values.back();
    }
}

SMALLMAP_TPL
void SMALLMAP_CLS::clear() {
    values.clear();
    unused.clear();
}

SMALLMAP_TPL
typename SMALLMAP_CLS::iterator SMALLMAP_CLS::begin() const {
    return iterator(&values, 0);
}
SMALLMAP_TPL
typename SMALLMAP_CLS::iterator SMALLMAP_CLS::end() const {
    return iterator(&values, values.size());
}
SMALLMAP_TPL
typename SMALLMAP_CLS::iterator SMALLMAP_CLS::iter(const typename SMALLMAP_CLS::Key& key) const {
    const Pair *const pair = this->get(key);
    if(pair == nullptr) return this->end();
    
    return iterator(&values, this->getID(pair));
}

SMALLMAP_TPL
std::string SMALLMAP_CLS::str() const {
    std::stringstream ss;
    ss << "{";
    bool first = true;
    for(Pair& pair : *this) {
        if(!first) ss << ", ";
        else first = false;
        if constexpr(!std::is_base_of<nvx::Gaussian_Root, Value>::value) {
            ss << pair.key << " = " << pair.value;
        }
    }
    ss << "}";
    return ss.str();
}

SMALLMAP_TPL
SMALLMAP_CLS::iterator::operator typename SMALLMAP_CLS::Key&() const {
    return ptr()->key;
}

SMALLMAP_TPL
void SMALLMAP_CLS::iterator::advance() {
    //std::cout << "advancing on " << vec << " from " << idx << " to " << (idx+1) << ", " << (vec? vec->size() : 0)<<"\n";
    idx++;
    if(ptr() != nullptr && vec->size() > idx && !ptr()->valid()) this->advance();
}
SMALLMAP_TPL
void SMALLMAP_CLS::iterator::retreat() {
    if(idx == 0) vec = nullptr;
    else idx--;
    
    if(ptr() != nullptr && !ptr()->valid()) this->retreat();
}
/*
SMALLMAP_TPL
bool SMALLMAP_CLS::iterator::equal(const typename SMALLMAP_CLS::iterator& o) const noexcept {
    
}
 * */
SMALLMAP_TPL
typename SMALLMAP_CLS::Pair* SMALLMAP_CLS::iterator::ptr() const {
    if(vec == nullptr || vec->size() == 0) return nullptr;
    else return (vec->data()) + idx;
}
