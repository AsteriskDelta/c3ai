#include "HistoryCache.h"
#include "SmallMap.h"
#include "History.h"
#include "Arith.h"
#define HTC_TPL template<typename V>
#define HTC_T HistoryCache<V>

HTC_TPL HTC_T::HistoryCache() : weight(0), consistence(0), ses(0), sek(0) {
    this->clear();
   // this->resetTimes();
}
HTC_TPL 
HTC_T::HistoryCache(const Self& o) : weight(o.weight), consistence(o.consistence), ses(o.ses), sek(o.sek), 
time(o.time), minTime(o.minTime), maxTime(o.maxTime) {
    const auto* optr = &o.range;
    for(Value *vptr = &range; vptr <= &sigma; vptr++, optr++) {
        (*vptr) = (*optr);
    }
}
HTC_TPL HTC_T::~HistoryCache() {
    
}

HTC_TPL
void HTC_T::clear() {
    weight = 0; consistence = 0;
    
    //Kids, don't try this at home.
    for(Value *vptr = &range; vptr <= &sigma; vptr++) {
        if constexpr(std::is_base_of<SmallMap_Root, Value>::value) vptr->clear();
        else if constexpr(std::is_arithmetic<Value>::value) vptr = 0;
        else {
            vptr->~Value();
            new(vptr) Value();
        }
    }
    
    this->resetTimes();
}

HTC_TPL
void HTC_T::accumulate(const Self& o, const Num& mult) {
    weight += o.weight * mult;
    consistence += o.consistence * mult;
    ses += o.ses * mult;
    sek += o.sek * mult;
    
    //const Num lMult = (mult < 0? -1 : 1) - mult;
    
    //Kids, listen to me...
    const auto* optr = &o.range;
    for(Value *vptr = &range; vptr <= &sigma; vptr++, optr++) {
        (*vptr) += (*optr) * mult;//((*optr) - (*vptr)) * lMult;
    }
    
    if(minTime > o.minTime) minTime = o.minTime;
    if(maxTime < o.maxTime) maxTime = o.maxTime;
    
    time = maxTime - minTime;
}

HTC_TPL
void HTC_T::resetTimes() {
    time = 0;
    minTime = std::numeric_limits<Time>::max();
    maxTime = -std::numeric_limits<Time>::max();
}

//WARNING: No longer used, sigma is now the pdf eval of the mean!!!
HTC_TPL
void HTC_T::calculateSigma() {
    maxVariance = range * range * 0.25;
    //Reciprocal (mask = proto variance:1) of normalized variance (vec 0...1)
    sigma = variance.mask() - (variance / maxVariance);
    //sigma = sigma * sigma;
    //Thus, a variance approaching 0 gives a sigma approaching 1
    
    consistence = sigma.magnitude() / 
    (sigma.ones() - sigma.zeros()).magnitude();
}
