#include "History.h"

#ifndef MOD_HTORY_IMPL_H
#ifdef MOD_HTORY_IMPL_H_2PASS
#define MOD_HTORY_IMPL_H
#define History VirtHistory
#endif

#define HT_TPL template<typename VALUE>
#define HT_T History<VALUE>

#include "CyclicVector.impl.h"
#include "Cascade.impl.h"
#include "Arith.h"


    HT_TPL HT_T::History() : Cyclic(64), Cascade_T(), cache(), cycleCache(), cascadeContrib(), cascadeWeightFactor(0.8), cycleSize(0), cycleFracPercent(0) {
        cache.dirty.range = true;
        cache.dirty.variance = true;
    }
    HT_TPL HT_T::~History()  {

    }

    HT_TPL
    void HT_T::setIdealWeight(const Num& weight) {
        constraints.weight = weight;
    }
    HT_TPL
    void HT_T::setMaxSamples(const unsigned int& cnt) {
        constraints.samples = cnt;
        this->setMaxSize(cnt);
    }

    HT_TPL
    bool HT_T::contains(const Time& t) const {
        return cache.minTime <= t && t <= cache.maxTime;
    }

    HT_TPL
    void HT_T::setCascadeSamples(const unsigned int& len, const unsigned int& fracPercent) {
        this->cycleLength = len;
        this->cycleFracPercent = fracPercent;
    }

    HT_TPL
    void HT_T::setCascadeFactor(Num factor) {
        cascadeWeightFactor = factor;
    }

    HT_TPL void HT_T::clear() {
        Cyclic::clear();
        if(this->cascaded()) this->cascade()->clear();
        cache.clear();
        cycleCache.clear();
        cascadeContrib.clear();
    }

    HT_TPL bool HT_T::frame() {
        //std::cout << "\tHIST FRAME sz=" << this->size() << ", empty=" << this->empty() << "\n";
        if(this->empty()) return false;

        const bool llDirty = cache.dirty.range || cache.dirty.variance;
        bool cascadeDirty = false;

        if(this->cascaded()) {
            if(this->cascade()->frame()) {
                cascadeDirty = true;
            }
        }
        if(cache.dirty.range || cache.dirty.variance) this->computeRange();
        if(cache.dirty.variance) this->computeVariance();

        _unused(cascadeDirty);
        this->getCascadeContrib(cascadeWeightFactor);

        return llDirty;
    }

    HT_TPL void HT_T::pop() {
        if(this->empty()) return;
        //running.duration = std::max(0.0, running.duration - data.front().duration / data.front().weight);
        Cyclic::pop();
        this->account(Cyclic::end(), false);
        cache.dirty.range = true;
    }

    HT_TPL void HT_T::push(const typename HT_T::Value& val, const Num& weight, const Time& t) {
        if(Cyclic::saturated()) {
            //std::cout << "Cycling val " << (std::prev(Cyclic::end())->value) <<" for cycle size " << cycleSize << "\n";
            this->account((Cyclic::end()), false);
            cache.dirty.range = true;
        }

        cache.min = ::min(val, cache.min);
        cache.max = ::max(val, cache.max);
        cache.range = cache.max - cache.min;

        //std::cout << "val= " << val.str() << ", weight=" << weight << ", t=" << t << "\n";
        auto newSample = Sample(val, weight, t);
        Cyclic::push(newSample);
        this->account(typename Cyclic::iterator(this, -1), true);
    }

    HT_TPL typename HT_T::Value HT_T::sample(const Time& t) {
        auto iter = this->begin();
        while(iter != this->end() && iter->time < t) ++iter;
        if(iter == this->end()) return Value();

        Sample *prevSample = &(*std::prev(iter)), *nextSample = iter.ptr();
        Num ratio = (t - prevSample->time).flt() / (nextSample->time - prevSample->time).flt();

        //std::cout << "Samples: " << prevSample->value << ", next=" << nextSample->value << " with ratio=" << ratio << "\n";
        //std::cout << "\tTimes: " << prevSample->time << " -> " << nextSample->time << "\n";
        return prevSample->value * (1.0-ratio) + nextSample->value * ratio;
    }

    HT_TPL typename HT_T::Value HT_T::evaluate(const Time& t) {
        //std::cout << "searching for " << t << " on cache of " << cache.minTime << " -> " << cache.maxTime << "\n";
        Value ret; Num rweight = 0;
        Time tMin = std::numeric_limits<Time>::max();
        Self *cas = this, *prevCas = nullptr;
        Num fpMult = 1000.0;//To avoid truncation on double FP

        Time lastMin = 0;
        while(cas != nullptr) {
            //if(cas->cache.minTime > tMin) break;

            if(cas->contains(t)) {
                Time casRange = cas->cache.maxTime - cas->cache.minTime;
                casRange *= casRange;
                const Num casWeight = (1.0 * fpMult) / casRange.flt();
                //return cas->sample(t);
                ret += cas->sample(t) * casWeight;
                rweight += casWeight;
            } else if(prevCas != nullptr && t < lastMin && t > cas->cache.maxTime) {//Interpolate between the two disparate cascades
                Time casRange = lastMin - cas->cache.maxTime;
                const Num prevCasFactor = ((t - cas->cache.maxTime) / casRange).flt();
                const Num casFactor = 1.0 - prevCasFactor;

                const Num casWeight = 1.0;//Since nothing else covers this space
                ret += (prevCas->sample(prevCas->cache.minTime) * prevCasFactor +
                    cas->sample(cas->cache.maxTime) * casFactor
                ) * casWeight;
                rweight += casWeight;

            }

            lastMin = cas->cache.minTime;
            tMin = std::min(tMin, cas->cache.minTime);
            prevCas = cas;

            if(cas->cascaded()) cas = cas->cascade();
            else cas = nullptr;
        }

        if(rweight == 0.0) {
            /*if(prevCas != nullptr && t < tMin) {//Historical extrapolation
                ret = prevCas->sample(prevCas->cache.minTime);
                Time dT = (prevCas->cache.minTime - t) / (prevCas->cache.maxTime - prevCas->cache.minTime);
                ret += prevCas->derivative() * dT;
            }*/
            return ret;
        } else return ret / rweight;
        /*if(t >= cache.minTime && t <= cache.maxTime) {
            //std::cout << "SAMPLING CACHE\n";
            auto iter = this->begin();
            while(iter != this->end() && iter->time < t) ++iter;

            Sample *prevSample = &(*std::prev(iter)), *nextSample = iter.ptr();
            Num ratio = (t - prevSample->time) / (nextSample->time - prevSample->time);
            //std::cout << "[lerp " << prevSample->value << " -> " << nextSample->value <<", fac="<<ratio<<"]\t";
            //std::cout << "pst="<<(prevSample->value * (1.0-ratio))<<", nxt=" << (nextSample->value * ratio) << "\t";

            return prevSample->value * (1.0-ratio) + nextSample->value * ratio;
        } else if(t < cache.minTime && this->cascaded()) {
            //std::cout << "Cascading from " << cache.minTime << " -> " << cache.maxTime << " to " << this->cascade()->cache.minTime << " -> " << this->cascade()->cache.maxTime << "\n";
            return this->cascade()->evaluate(t);
        } else {
            //std::cerr << "Out of range time requested: " << t << ", last range was " << cache.minTime << " -> " << cache.maxTime << "\n";
            return Value();
        }*/
    }


    HT_TPL void HT_T::setCascadeContrib(const typename HT_T::Cache& oc) {
        //std::cout << "SET CASCADE " << oc.weight << " -> " << oc.mean << "\n";
        //if(false)
        //if(cascadeContrib) {
        cache.accumulate(cascadeContrib, -cascadeWeightFactor);
        //}

        this->cascadeContrib = oc;
        cache.accumulate(cascadeContrib, cascadeWeightFactor);

        this->computeRange();
        this->computeVariance();
    }

    HT_TPL void HT_T::computeRange() {
        //;//cache.min.clear(); cache.max.clear();
        //cache.min = std::numeric_limits<Value>::max();
        //cache.max = -std::numeric_limits<Value>::max();
        cache.min = cache.max = this->begin()->value;
        cache.minTime = this->begin()->time;//std::numeric_limits<Time>::max();
        cache.maxTime = this->begin()->time;//-std::numeric_limits<Time>::max();

        for(const Sample& sample : *this) {
            cache.min = ::min(cache.min, sample.value);
            cache.max = ::max(cache.max, sample.value);
            cache.minTime = std::min(cache.minTime, sample.time);
            cache.maxTime = std::max(cache.maxTime, sample.time);
        }

        cache.range = cache.max - cache.min;
        cache.maxVariance = cache.range * cache.range * 0.25;
        cache.dirty.range = false;
    }
    HT_TPL void HT_T::computeVariance() {
        if(cache.weight == 0 || cache.maxVariance.magnitude2() == 0.0) {
            cache.variance.clear();
            //std::cout << "\tclearing history cache because " << cache.weight << " == 0 || " << cache.maxVariance << " mag == 0\n";
            cache.sigma.clear();
            return;
        }

        //std::cout << "COMP VAR for weight=" << cache.weight << "\n";
        Value totalVar = Value();
        Num totalWeight = 0;
        //std::cout << "Made value at " << &totalVar << ", marked itself as " << totalVar.parent << "\n";
        for(Sample& sample : *this) {
            Value tmp = (sample.value - cache.mean);// * sample.weight;
            //std::cout << "\t\t\tVARI adding " << sample.value <<" vs " << cache.mean << " -> " << tmp << " * " << sample.weight << "\n";
            totalVar += (tmp * tmp) * sample.weight;
            totalWeight += sample.weight;
            //tmp.clear();
        }
        cache.weight = totalWeight;
        //std::cout << "\ttotalVar[" << totalVar.size() << "]=" << totalVar << " / " << cache.weight << "\n";
        //std::cout  << "PARENT SHOULD BE " << &totalVar << "\n";
        if(totalWeight <= 0) cache.variance = Value();
        else cache.variance = totalVar / totalWeight;

        Value stdd = cache.stddev = sqrt(cache.variance);
        cache.epsilon = stdd * SignificanceDeviation;

        Value totalSkew, totalKurtosis;
        for(Sample& sample : *this) {
            Value tmp = (sample.value - cache.mean);// * sample.weight;

            totalSkew += (tmp*tmp*tmp) * sample.weight;
            totalKurtosis += (tmp*tmp*tmp*tmp) * sample.weight;
        }
        totalSkew *= ((sqrt(totalWeight * (totalWeight - 1.0)) / (totalWeight - 2))  / totalWeight);//Adjustment factor
        totalSkew /= (stdd*stdd*stdd);
        cache.skew = totalSkew;

        totalKurtosis /= (stdd * stdd * stdd * stdd) * totalWeight;
        cache.kurtosis = totalKurtosis - 3;

        //See standard error of skew/kurtosis formulae
        cache.ses = sqrt(
            ( (6 * totalWeight * (totalWeight - 1)) / ((totalWeight - 2) * (totalWeight + 1) * (totalWeight + 3)) )
        );
        cache.sek = Num(2) * cache.ses * sqrt(
            (totalWeight*totalWeight - 1.0) / ((totalWeight - 3.0) * (totalWeight + 5.0))
        );

        //std::cout << "\t\tCALC VARI from " << totalVar << " / " << cache.weight << " = " << cache.variance<<"\n";
        /*
        //Reciprocal (mask = proto variance:1) of normalized variance (vec 0...1)
        cache.sigma = cache.variance.mask() - (cache.variance / cache.maxVariance);
        //Thus, a variance approaching 0 gives a sigma approaching 1

        cache.consistence = cache.sigma.magnitude() /
                            (cache.sigma.ones() - cache.sigma.zeros()).magnitude();
        */
        //cache.calculateSigma();

        cache.dirty.variance = false;
    }

    HT_TPL
    typename HT_T::Cache& HT_T::getCascadeContrib(Num factor) {
        if(!this->cascaded()) return (this->cascadeContrib = this->cache);

        const Num rFactor = factor;
        //factor = sgn(factor) * (1.0 / (1.0 + fabs(factor * std::max(Num(1), this->cascade()->cache.weight / cache.weight) )) );
        //factor *= cache.weight / (cache.weight + this->cascade()->cache.weight);
        const Cache& contrib = this->cascade()->getCascadeContrib(rFactor);
        factor = cache.weight / (contrib.weight + cache.weight);
        factor = 1*(1.0-rFactor) + (rFactor)*factor;
        //std::cout << "\taccumulating caches with ratio " << factor << " from w=" << cache.weight << "ow=" << contrib.weight;
        //TODO: Check if contents has changed, return previous instead of regenerating
        cascadeContrib.clear();
        cascadeContrib.accumulate(contrib, 1.0 - factor);
        cascadeContrib.accumulate(this->cache, factor);
        //cascadeContrib.calculateSigma();
        //std::cout << " for sigma="<< cascadeContrib.sigma << ", mean=" << cascadeContrib.mean << ", weight=" << cascadeContrib.weight << "\n";
        return cascadeContrib;
    }

    HT_TPL void HT_T::account(typename HT_T::Cyclic::iterator sample, bool additive) {
        if(!sample) return;
        //Sample removal is now handled within frame()s updateVariance
        const Num mult = additive? 1.0 : 0.0;//-1.0;
        auto prevSample = std::prev(sample);
        //std::cout << "acc iter with parent=" << sample.vec << ", idx=" << sample.idx << " = " << sample.ptr() << "\n";
        //std::cout << "sib:" << prevSample.vec << " at idx=" << prevSample.idx << " -> " << prevSample.ptr() << "\n";

        const Num sampleWeight = sample->weight * mult;
        cache.weight += sampleWeight;



        //std::cout << "\tweights: cache=" << cache.weight << ", cycle=" << cycleCache.weight << "\n";
        const Num sampleFrac = sampleWeight / cache.weight;

        cache.time = (1.0 - sampleFrac) * cache.time.flt() + (sampleFrac) * sample->time.flt();

        cache.mean = sample->value * sampleFrac + cache.mean * (1.0 - sampleFrac);

        //std::cout << "VAL=" << sample->value <<": running mean=" << cache.mean << " x" << sampleFrac << "\t\t cycle mean=" << cycleCache.mean << " x" << sampleCycleFrac <<"\n";

        cache.integral += sample->value * (sampleWeight);
        cache.derivative += (sample->value - prevSample->value) *
                            ((prevSample->weight + sample->weight) / 2.0 * mult);

        if(additive) {
            cache.max = ::max(cache.max, sample->value);
            cache.min = ::min(cache.min, sample->value);
            cache.minTime = std::min(cache.minTime, sample->time);
            cache.maxTime = std::max(cache.maxTime, sample->time);
        } else {
            //Handle cascade of discarded values
            cycleCache.weight += sample->weight;
            const Num sampleCycleFrac = sample->weight / cycleCache.weight;
            cycleCache.time = (1.0 - sampleCycleFrac) * cycleCache.time.flt() + (sampleCycleFrac) * sample->time.flt();
            cycleCache.mean = sample->value * sampleCycleFrac + cycleCache.mean * (1.0 - sampleCycleFrac);
            cycleSize++;
            //std::cout << "added to cycle for size " << cycleSize << "/" << this->cycleLength <<"\n";
            if(this->shouldCycle()) this->submitCycle();


        }

        cache.dirty.variance = true;

        /*if(this->cycled()) {
            if(!this->cascaded()) {
                this->cascade()->setMaxSamples(constraints.samples);
                this->cascade()->setCascadeSamples(this->cycleLength);
            }

            this->cascade()->push(cycleCache.mean, cycleCache.weight, cycleCache.time);
            //std::cout << "Adding cycle value " << cycleCache.mean << " * " << cycleCache.weight << " at " << cycleCache.time << "\n";
            cycleCache.~Cache();
            new(&cycleCache) Cache();
            //cycleCache = {};
            //std::cout << "CYCLING!!!\n";
        }*/

    }

    HT_TPL bool HT_T::shouldCycle() {
        if(cycleSize >= this->cycleLength) return true;
        else if(cycleSize+1 >= this->cycleLength && (rand() % 100) < cycleFracPercent) return true;
        else return false;
    }


    HT_TPL void HT_T::submitCycle() {
        //std::cout << "SUBMITTED CASCADE cycle, cycleSize=" << cycleSize << ", len=" << cycleLength << "\n";
        if(!this->cascaded()) {//Initialize cased if it doesn't already exist
            this->cascade()->setMaxSamples(constraints.samples);
            this->cascade()->Cyclic::setCycleSize(this->cycleLength);
            this->cascade()->setCascadeSamples(this->cycleSize, this->cycleFracPercent);
            this->cascade()->setCascadeFactor(this->cascadeWeightFactor);
        }

        this->cascade()->push(cycleCache.mean, cycleCache.weight, cycleCache.time);
        //std::cout << "Adding cycle value " << cycleCache.mean << " * " << cycleCache.weight << " at " << cycleCache.time << "\n";
        //cycleCache.~Cache();
        //new(&cycleCache) Cache();
        cycleCache.clear();
        cycleSize = 0;
        cache.dirty.range = true;
    }

    /*HT_TPL void HT_T::proxy(const typename HT_T::Cache& oc) {
        Num factor = cache.weight / (contrib.weight + cache.weight);

        cache.accumulate(contrib, 1.0 - factor);
        cascadeContrib.accumulate(this->cache, factor);
    }*/

#ifndef MOD_HTORY_IMPL_H_2PASS
#define MOD_HTORY_IMPL_H_2PASS
#include "HistoryCache.impl.h"
#include "History.impl.h"
#else
#undef History
#undef History
#endif

#endif /* HTORY_H */
