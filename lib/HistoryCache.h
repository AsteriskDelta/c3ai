#ifndef HISTORY_CACHE_H
#define HISTORY_CACHE_H
#include "C3AI.h"
#include <type_traits>

#include "CyclicVector.h"

template<typename V>
class HistoryCache {
public:
    typedef V Value;
    typedef HistoryCache<Value> Self;
    typedef CyclicVector<V> Cyclic;
    
    HistoryCache();
    HistoryCache(const Self& o);
    ~HistoryCache();
    
    void clear();
    
    void accumulate(const Self& o, const Num& mult);
    
    void calculateSigma();
    
    Num weight, consistence;
    
    //Standard error skew/kurtosis
    Num ses, sek;
    
    Value range, min, max;
    
    Value mean;
    Value derivative, integral;
    
    Value variance, maxVariance, stddev;
    Value epsilon;
    Value skew, kurtosis;
    Value sigma;
    
    
    Time time;
    Time minTime, maxTime;
    
    struct {
        bool range : 1;
        bool variance : 1;
        unsigned char padd : 6;
    } dirty;
protected:
    void resetTimes();
};

#endif
