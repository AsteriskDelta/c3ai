#ifndef C3AI_H
#define C3AI_H

#include "C3Include.h"


namespace C3AI {
    //typedef uint64_t SymbolID;
    class Symbol;
    class Hyperpoint;
}

#include "symbol/Intrinsic.h"
#include "symbol/Symlink.h"
#include "extension/ExtManager.h"


#include "core/AI.h"
#include "critic/Critic.h"
#include "interface/Interface.h"

#endif
