#include "main.h"

static std::string DefaultAI_ID = "gamma";
std::string DefaultCorePath = "../cores/";
std::string DefaultExtensionPath = "../extensions/";
std::string AI_ID;
AI *Active = nullptr;

void exit() {
    
}

int main(int argc, char** argv) {
    bool absCorePath = false;
    std::string argCorePath = "";
    if(argc >= 2) {
        if(AI_ID[0] == '/') {
            absCorePath = true;
            argCorePath = std::string(argv[1]);
            AI_ID = std::get_last_delim(argCorePath, "/");
        } else {
            AI_ID = std::string(argv[1]);
        }
    } else {
        AI_ID = DefaultAI_ID;
    }
    
    Application::BeginInitialization();
    //System::SetrLimit(1024*1024*1024);
    Application::SetDataDir("../../data/");
    Application::SetName("ai-"+AI_ID);
    Application::SetCodename("ai-"+AI_ID);
    Application::SetCompanyName("Delta III Tech");
    Application::SetVersion("0.0r1X");
    Application::SetDebug(true);
    Application::OnExit(&exit);
    //System::SetStackLimit(1024*1024*256);
    Application::EndInitialization();
    
    std::string corePath = (absCorePath? argCorePath : DefaultCorePath + AI_ID) + "/" + AI_ID + ".xml";
    Console::Out("Init AI [",AI_ID,"] from ",corePath);
    
    Active = new AI(corePath, DefaultExtensionPath);
    
    return 0;
}

