#ifndef C3AI_BOOT_CORE_H
#define C3AI_BOOT_CORE_H
#include "C3AI.h"
#include <ARKE/Application.h>

using namespace C3AI;
using namespace arke::Application;

extern std::string AI_ID, DefaultCorePath;
extern AI *Active;

#endif
