#ifndef HISTORY_HDGAUSSIAN_H
#define HISTORY_HDGAUSSIAN_H
#include "C4AI.h"
#include <type_traits>

#include <NVX/NGaussian.h>
#include "SmallMap.h"

template<typename PT>
class HDGaussian : public 
SmallMap<unsigned int, nvx::Gaussian<typename PT::value>> {
public:
    typedef PT Point;
    typedef nvx::Gaussian<typename PT::value> Dist;
    typedef SmallMap<unsigned int, nvx::Gaussian<typename PT::value>> Map;
    
    HDGaussian();
    ~HDGaussian();
    
    void set(const Point& mean, const Point& variance);
    
    Point pdf(const Point& state, bool rel = false) const;
    Point cdf(const Point& state) const;
    
    Point invpdf(const Point& state) const;
    Point invpdf(const Num& chance) const;
    
    //Point stddev() const;
    
    
    Num pdfProduct(const Point& state, bool rel = false) const;
protected:
    struct {
        Point mean, variance;
        Point meanProb;
    } cache;
    bool invalid;
};

#endif
