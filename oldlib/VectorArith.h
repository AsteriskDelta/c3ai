#ifndef VECTORARITH_H
#define VECTORARITH_H
#include "C3AI.h"
#include <type_traits>

enum class enabler_t {};

template<bool T>
using EnableIf = typename std::enable_if<T, enabler_t>::type;

#define VEC_TOK(x) x
#define VEC_CAT2(a,b) VEC_TOK(a b)
#define VEC_CAT(a,b) VEC_CAT2(VEC_TOK(a),VEC_TOK(b))
#define VEC_GEN_OP(OP, UNION_OP, INTER_OP, UNION, INTERSECTION) \
template<typename T, EnableIf<std::is_base_of<VectorArith_Root, T>::value>...>\
inline auto VEC_CAT(operator,OP)(const T& o) const {\
    typedef typename std::remove_reference<decltype(*parent)>::type RT;\
    RT ret = RT();\
    for (auto it = parent->begin(); it != parent->end(); ++it) {\
        if(INTERSECTION && !o.has(it->key)) continue;\
        ret[it] =  (it->value) OP (o[it->key]) ;\
        if(_isnan(ret[it])) ret[it] = 0;\
    }\
    if(UNION) {\
        for (auto ot = o.begin(); ot != o.end(); ++ot) {\
            if (ret.has(ot->key)) continue;\
            ret[ot->key] = UNION_OP(ot->value);\
        }\
    }\
    return ret;\
}\
template<typename T, EnableIf<!std::is_base_of<VectorArith_Root, T>::value>...>\
inline auto VEC_CAT(operator,OP)(const T& o) const {\
    typedef typename std::remove_reference<decltype(*parent)>::type RT;\
    RT ret = RT();\
    std::cout << "OP ON VEC of size " << parent->size() << "\n";\
    for (auto it = parent->begin(); it != parent->end(); ++it) {\
        std::cout << " iter is " << it.vec << " at idx=" << it.idx << "\n";\
            ret[it] =  (it->value) OP (o) ;\
    }\
    return ret;\
}

#define EQNAME2(NM) NM##=
#define EQNAME(NM) EQNAME2(NM)
#define VEC_OP_EQ(OP) \
template<typename T>\
inline auto& EQNAME(VEC_CAT(operator,OP))(const T& o) {\
    (*parent) = (*parent) OP (o);\
    return *parent;\
}

    struct VectorArith_Root{};
    
    template<typename T, typename I, EnableIf<!std::is_base_of<VectorArith_Root, T>::value>...>
    inline auto GetValue(const T& val, const I& idx) {
        _unused(idx);
        return val;
    }
    template<typename T, typename I, EnableIf<std::is_base_of<VectorArith_Root, T>::value>...>
    inline auto GetValue(const T& val, const I& idx) {
        return val[idx];
    }
    template<typename T, typename I, EnableIf<!std::is_base_of<VectorArith_Root, T>::value>...>
    inline auto HasValue(const T& val, const I& idx) {
        _unused(idx); _unused(val);
        return true;
    }
    template<typename T, typename I, EnableIf<std::is_base_of<VectorArith_Root, T>::value>...>
    inline auto HasValue(const T& val, const I& idx) {
        return val.has(idx);
    }
    /*
    template<typename TA, typename TB, typename T3 = void>
    struct va_type;
    
    template<typename TA, typename TB>
    struct va_type <TA,TB, typename std::enable_if<std::is_base_of<VectorArith_Root, TA>::value, void>::type> {
        typedef TA type;
    };
    template<typename TA, typename TB>
    struct va_type <TA,TB, typename std::enable_if<std::is_base_of<VectorArith_Root, TB>::value, void>::type> {
        typedef TB type;
    };*/
    
#define VA_EXT_OP(OP, UNION_OP, INTER_OP, UNION, INTERSECTION) \
    template<typename TA, typename TB, EnableIf<std::is_base_of<VectorArith_Root, TA>::value >...>\
    inline auto VEC_CAT(operator,OP)(const TA& a, const TB& b) {\
        typedef TA RET_T;\
        RET_T ret = RET_T();\
        for (auto it = a.begin(); it != a.end(); ++it) {\
            if constexpr(it.hasKey() && INTERSECTION) if(!HasValue(b, it->key)) continue;\
            ret[it] = (it->value) INTER_OP (GetValue(b, it->key));\
            if constexpr(std::is_floating_point<decltype(ret[it])>::value) {\
                if(_isnan(ret[it])) ret[it] = 0;\
            }\
        }\
        constexpr const bool couldUnion = std::is_base_of<VectorArith_Root, TB>::value;\
        if constexpr(couldUnion && UNION) {\
            for (auto ot = b.begin(); ot != b.end(); ++ot) {\
                if constexpr(ot.hasKey()) if (ret.has(ot->key)) continue;\
                ret[ot->key] = UNION_OP(ot->value);\
            }\
        }\
        return ret;\
    }
    
#define EQNAME2(NM) NM##=
#define EQNAME(NM) EQNAME2(NM)
#define VA_EXT_EQ(OP) \
    template<typename T>\
    inline auto& EQNAME(VEC_CAT(operator,OP))(const T& o) {\
        (*this) = (*this) OP (o);\
        return *this;\
    }
    
#define VA_ACCUM_OP(OP, UNION_OP, INTER_OP, UNION) \
    template<typename TA, typename TB, EnableIf<std::is_base_of<VectorArith_Root, TA>::value >...>\
    inline bool VEC_CAT(operator,OP)(const TA& a, const TB& b) {\
        typedef bool RET_T;\
        RET_T ret = RET_T();\
        for (auto it = a.begin(); it != a.end(); ++it) {\
            ret EQNAME(UNION_OP) (it->value) INTER_OP (GetValue(b, it->key));\
        }\
        constexpr const bool couldUnion = std::is_base_of<VectorArith_Root, TB>::value;\
        if constexpr(couldUnion && UNION) {\
            for (auto ot = b.begin(); ot != b.end(); ++ot) {\
                if constexpr(ot.hasKey()) if (a.has(ot->key)) continue;\
                ret EQNAME(UNION_OP) false INTER_OP false;\
            }\
        }\
        return ret;\
    }
    /*
    template<typename TA, typename TB, EnableIf<!std::is_base_of<VectorArith_Root, TA>::value &&\ std::is_base_of<VectorArith_Root, TB>::value >...>\
    inline auto VEC_CAT(operator,OP)(const TA& a, const TB& b) {\
        typedef typename va_type<TA,TB>::type RET_T;\
        for (auto it = parent->begin(); it != parent->end(); ++it) {\
            if(INTERSECTION && !o.has(it->key)) continue;\
                ret[it] =  (it->value) OP (o[it->key]) ;\
        }\
        if(UNION) {\
            for (auto ot = o.begin(); ot != o.end(); ++ot) {\
                if (ret.has(ot->key)) continue;\
                    ret[ot->key] = UNION_OP(ot->value);\
            }\
        }\
        return ret;\
    }\
    */
    VA_EXT_OP(+, ,+, true, false);
    VA_EXT_OP(-,-,-, true, false);
    VA_EXT_OP(*, ,*, false, true);
    VA_EXT_OP(/, ,/, false, true);
    
    VA_EXT_OP(&, ,;, false, true);
    VA_EXT_OP(|, ,;, true, false);
    
    VA_ACCUM_OP(==,&,==,true);
    VA_ACCUM_OP(!=,|,!=,false);
    VA_ACCUM_OP(>,&,>,true);
    VA_ACCUM_OP(<,&,<,true);
    VA_ACCUM_OP(>=,&,>=,true);
    VA_ACCUM_OP(<=,&,<=,true);

    template<typename PAR_T>
    struct VectorArith  : public VectorArith_Root {
        typedef PAR_T Parent;
        typedef VectorArith<PAR_T> Self;
        Parent parent;
        
        inline auto& self() const noexcept {
            return *parent;
        }
        
        VectorArith() = delete;
        inline explicit VectorArith(Parent par) : parent(par) {};
        
        /*VEC_GEN_OP(+, ,+, true, false);
        VEC_GEN_OP(-,-,-, true, false);
        //VEC_GEN_OP(*, ,*, false, true);
        //VEC_GEN_OP(/, ,/, false, true);
        
        VEC_GEN_OP(&, ,;, false, true);
        VEC_GEN_OP(|, ,;, true, false);
        */
        /*VEC_OP_EQ(+);
        VEC_OP_EQ(-);
        VEC_OP_EQ(*);
        VEC_OP_EQ(/);
        VEC_OP_EQ(&);
        VEC_OP_EQ(|);*/
        
        /*
        template<typename T>
        inline typename std::enable_if<std::is_base_of<VectorArith_Root, T>::value, typename std::remove_reference<
        typename std::remove_pointer<Parent>::type
        >::type>::type operator*(const T& o) const {
            typedef typename std::remove_const<typename std::remove_reference<
            typename std::remove_pointer<Parent>::type
            >::type>::type RT_T;
            RT_T ret = RT_T();
            std::cout << "PAR=" << parent << "\n";
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                if(!o.has(it->key)) continue;
                    ret[it] =  (it->value) * (o[it->key]) ;
            }
            return ret;
        }
        template<typename T>
        inline typename std::enable_if<!std::is_base_of<VectorArith_Root, T>::value, 
        typename std::remove_reference<
        typename std::remove_pointer<Parent>::type
        >::type>::type operator*(const T& o) const {
            typedef typename std::remove_const<typename std::remove_reference<
            typename std::remove_pointer<Parent>::type
            >::type>::type RT_T;
            RT_T ret = RT_T();
            std::cout << "PAR=" << parent << "\n";
            std::cout << "* ON VEC of size " << parent->size() << ", parent=" << parent << "\n";
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                std::cout << " iter is " << it.vec << " at idx=" << it.idx << "\n";
                ret[it] =  (it->value) * (o) ;
            }
            return ret;
        }
        
        
        template<typename T, EnableIf<std::is_base_of<VectorArith_Root, T>::value>...>
        inline auto operator/(const T& o) const {
            typedef typename std::remove_reference<decltype(*parent)>::type RT;
            RT ret = RT();
            std::cout << "PAR=" << parent << "\n";
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                if(!o.has(it->key)) continue;
                ret[it] =  (it->value) / (o[it->key]) ;
            }
            return ret;
        }
        template<typename T, EnableIf<!std::is_base_of<VectorArith_Root, T>::value>...>
        inline auto operator/(const T& o) const {
            typedef typename std::remove_reference<decltype(*parent)>::type RT;
            RT ret = RT();
            std::cout << "PAR=" << parent << "\n";
            std::cout << "/ ON VEC of size " << parent->size() << "\n";
            std::cout << "BEGIN=" << parent->begin().idx << ", END=" << parent->end().idx << "\n";
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                std::cout << " iter is " << it.vec << " at idx=" << it.idx << "\n";
                ret[it] =  (it->value) / (o) ;
            }
            return ret;
        }
        */
        /*
        Num magnitude() const;
        Num magnitude2() const;
        
        Num distanceTo(const Vec &o) const;
        Num distanceTo2(const Vec &o) const;
        
        Num relDistanceTo(const Vec &o) const;
        Num relDistanceTo2(const Vec &o) const;
        
        Num dot(const Vec& ot) const;
        
        Vec& makeRelative(bool raw = false);
        Vec& clamp();
        
        Vec nonZero() const;
        
        inline explicit operator bool() const {
            return this->magnitude() > 0.001;
        }*/
        /*
        template<class=void>
        inline auto magnitude2() const noexcept {
            Num ret = 0.0;
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                ret += static_cast<Num>((it->value) * (it->value));
            }
            return ret;
        }
        template<class=void>
        inline auto magnitude() const {
            return sqrt(this->magnitude2());
        }
        template<typename T>
        inline auto distanceTo2(const T& o) const {
            return (o - self()).magnitude2();
        }
        template<typename T>
        inline auto distanceTo(const T& o) const {
            return (o - self()).magnitude();
        }

        template<typename T>
        inline auto dot(const T& o) const noexcept {
            Num ret = 0.0;
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                ret += static_cast<Num>((*it) * o[it.key]);
            }
            return ret;
        }
        
        template<class=void>
        inline auto mask() const noexcept {
            auto ret = *parent;
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                ret[it->key] = 1.0;
            }
            return ret;
        }
        template<class=void>
        inline auto ones() const noexcept {
            return this->mask();
        }
        template<class=void>
        inline auto zeros() const noexcept {
            auto ret = *parent;
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                ret[it->key] = 0.0;
            }
            return ret;
        }*/
    };


#endif /* VECTORARITH_H */

