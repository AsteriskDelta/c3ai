#ifndef C3AI_SYMBOLSET_H
#define C3AI_SYMBOLSET_H

namespace C3AI { 
    class SymbolSet {
    public:
        SymbolSet(SymbolSet *par);
        ~SymbolSet();
        
        SymbolPtr registerIntrinsic(Intrinsic *newIntrin);
    protected:
        SymbolSet *parent = nullptr;
        std::vector<SymbolSet*> subsets;
        std::vector<SymbolPtr> symbols;
        std::vector<Intrinsic*> natives;
        
        /*FIDVector*/ std::list<Symbol*> prospectives;
    };
    
};
#endif
