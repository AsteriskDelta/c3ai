#ifndef C3AI_MUSE_H
#define C3AI_MUSE_H

namespace C3AI {
    class Muse {
    public:
        Muse();
        ~Muse();
        
        
    protected:
        AI *ai;
        CriticSet *critics;
    };
};

#endif
