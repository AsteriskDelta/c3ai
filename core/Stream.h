#ifndef C3AI_STREAM_H
#define C3AI_STREAM_H
#include "SymbolSet.h"
#include "Muse.h"

namespace C3AI {
    //Duality with RegionStream
    class Stream  : public SymbolSet, public Muse {
    public:
        Stream(Stream *par, SymbolSet *syms);
        ~Stream();
        
        void addRegion(Region&& region);
    protected:
        Stream *parent;
        std::vector<Stream*> substreams;
    };
};

#endif
