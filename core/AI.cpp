#include "AI.h"
#include <ARKE/XML.h>

namespace C3AI {
    thread_local AI *ActiveAI = nullptr;
    
    AI::AI(const std::string& nPath, const std::string& ePath) : ExtManager(ePath), CriticSet(), Stream(nullptr, nullptr), xmlPath_(nPath) {
        Muse::ai = this;
        Muse::critics = this;
        ActiveAI = this;
        this->loadCoreXML();
    }
    
    AI::~AI() {
        
    }
    
    bool AI::loadCoreXML() {
        XMLDoc doc;
        if(!doc.load(xmlPath_)) {
            Console::Err("AI Core XML file at ",xmlPath_," could not be parsed... Aborting.");
            return false;
        }
        
        XMLNode root = doc.child("ai");
        if(!root) {
            Console::Err("AI Core XML at ",xmlPath_," does not have ai tag");
            return false;
        }
        
        this->id_ = root.get<std::string>("id");
        Console::Out("AI to be loaded: [",this->id(),"]");
        
        for(XMLNode lib = root["baseExts"]["ext"]; bool(lib); lib = lib.sibling()) {
            //std::cout << "iter of baseExt.ext\n";
            auto extID = lib.get<std::string>("id");
            if(extID.empty()) continue;
            //std::cout << "\t\t"<<extID<<"\n";
            Extension::LoadData data;
            data.id = extID;
            
            for(XMLNode arg = lib["arg"]; bool(arg); arg = arg.sibling()) {
                data.args[arg.get<std::string>("id")] = arg.get<std::string>();
            }
            
            if(!ExtManager::requestLink(data)) {
                Console::Err("\tFailed to link-in extension library ",extID);
            }
        }
        
        return true;
    }
    
};
