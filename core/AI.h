#ifndef C3AI_AI_CORE_H
#define C3AI_AI_CORE_H
#include "ExtManager.h"
#include "CriticSet.h"
#include "Stream.h"

namespace C3AI {
    
    class AI  : public ExtManager, public CriticSet, public Stream {
    public:
        AI(const std::string& nPath, const std::string& extPath);
        ~AI();
        
    protected:
        std::string id_, name_, path_, xmlPath_;
        
        std::vector<SymbolSet*> globalSyms;
        std::unordered_map<std::string, SymbolSet*> globalSymsIndex;
        
        bool loadCoreXML();
        
        
    public:
        _getters(path, id, name);
    };
};
#endif
