#include "SymbolSet.h"

namespace C3AI {
    SymbolSet::SymbolSet(SymbolSet *par) : parent(par), subsets() {
        
    }
    
    SymbolSet::~SymbolSet() {
        
    }

    SymbolPtr SymbolSet::registerIntrinsic(Intrinsic *newIntrin) {
        natives.push_back(newIntrin);
        symbols.push_back((Symbol*)newIntrin);
        return symbols.back();
    }
};
