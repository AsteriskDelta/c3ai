#pragma once
#ifndef EXTENSION_INC_INC_H
#define EXTENSION_INC_INC_H

#include "Extension.h"
namespace C3AI {
    class ExtLibrary;
};
#define EXT_LINK_CONCAT_PP(A,B) A ## B
#define EXT_LINK_CONCAT(A,B) EXT_LINK_CONCAT_PP(A,B)
#define ON_EXTENSION_LINK() extern "C" void EXT_LINK_CONCAT(CAI_EXTENSION_ID,_OnLink) (C3AI::ExtLibrary *const par, const C3AI::Extension::LoadData& lData)
#define ON_EXTENSION_UNLINK() extern "C" void EXT_LINK_CONCAT(CAI_EXTENSION_ID,_OnUnlink) ()


#endif
