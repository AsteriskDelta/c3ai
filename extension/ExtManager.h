#ifndef C3AI_EXT_MANAGER_H
#define C3AI_EXT_MANAGER_H
#define ARKE_LOADABLE_V2

#include "C3Include.h"
#include "ExtLibrary.h"
#include <ARKE/LoadableManager.h>

namespace C3AI {
    class ExtLibrary;
    class Extension;
    
    class ExtManager : public LoadableManager<ExtLibrary> {
    public:
        friend class ExtLibrary;
        friend class Extension;
        typedef LoadableManager<ExtLibrary> Super;
        
        ExtManager(const std::string& ePath);
        virtual ~ExtManager();
        
        bool canLink(ExtLibrary *lib);
        bool requestLink(const Extension::LoadData& eld);
        bool requestLink(ExtLibrary *lib, const Extension::LoadData& eld);
        
        void wasLinked(ExtLibrary *lib);
        void wasUnlinked(ExtLibrary *lib);
        
        virtual void wasLoaded(ExtLibrary* ptr) override;
        
        inline const std::string& extensionPath() const {
            return extPath;
        }
        inline const std::string& extensionLibPath() const {
            return extLibPath;
        }
        
        std::unordered_set<ExtLibrary*> extensions;
        std::unordered_set<ExtLibrary*> linked;
        std::unordered_map<std::string, ExtLibrary*> byID;
        
        std::unordered_map<std::string, std::unordered_set<ExtLibrary*>> byType;
    protected:
        std::unordered_map<std::string, ExtLibrary*> blacklisted;
        
        std::string extPath, extLibPath;
        
    };
    
    //extern ExtManager *Extensions;
};

#endif
