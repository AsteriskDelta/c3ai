#include "Extension.h"
#include "ExtLibrary.h"

namespace C3AI {
    Extension::Extension(ExtLibrary *par, const LoadData& lData) : parent_(par), loadData_(lData) {
        parent_->addExtension(this);
    }
    
    Extension::Extension::~Extension() {
        if(parent_ != nullptr) parent_->removeExtension(this);
    }
};
