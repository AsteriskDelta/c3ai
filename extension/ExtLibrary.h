#ifndef C3AI_EXT_LIB_H
#define C3AI_EXT_LIB_H
#include "C3Include.h"
#include <ARKE/Loadable.h>
#include <unordered_set>
#include "Extension.h"

namespace C3AI {
    class ExtManager;
    class Extension;
    
    class ExtLibrary :  public Loadable {
    friend class ExtManager;
    friend class Extension;
    public:
        typedef ExtLibrary Self;
        
        ExtLibrary();
        ExtLibrary(const std::string& p);
        virtual ~ExtLibrary();
        
        virtual bool couldLoad(const std::string& p) const override;
        virtual bool load(const std::string& p) override;
        virtual void unload() override;
        
        virtual std::string getName() const override;
        _getter(name);
        _getter(id);
        _getter(type);
        _getter(xmlPath);
        
        virtual std::string getChildID(const std::string& i) const;
        
        inline virtual void* rawInstancePtr() override { return reinterpret_cast<void*>(this); };
        
        virtual bool isLoaded() const;
        
        static Self* Factory(const std::string p = "");
        
        bool link(const Extension::LoadData& lData);
        bool unlink();
        inline bool isLinked() const {
            return this->linked;
        }
        
        struct LibData {
            std::string path, prefix;
            void *handle;
            bool absolutePath;
            
            std::string getFullPath(const ExtManager *const manager) const;
        };
        std::list<LibData> libs;
        
        std::list<std::string> deps, conflicts;//, provides;
    
    protected:
        bool linked;
        std::string id_, name_, type_, xmlPath_, idPath_, idSub_;
        std::unordered_set<Extension*> extensions;
        ExtManager *manager_;
        
        void addExtension(Extension* ext);
        void removeExtension(Extension *ext);
    public:
        _getter(manager);
    };
};

#endif

