#include "ExtManager.h"
#include "ExtLibrary.h"

//TODO: Blacklisting backend

namespace C3AI {
    thread_local ExtManager *CurrentExtManager = nullptr;
    
    ExtManager::ExtManager(const std::string& ePath) : ExtManager::Super(), extPath(ePath), extLibPath("") {
        //Extensions = this;
        CurrentExtManager = this;
        Super::loadFromDir(ePath);
    }
    
    ExtManager::~ExtManager() {
        //if(Extensions == this) Extensions = nullptr;
    }
    
    bool ExtManager::canLink(ExtLibrary *lib) {
        if(lib == nullptr) return false;
        
        auto it = blacklisted.find(lib->id());
        if(it != blacklisted.end()) return false;
        
        return true;
    }
    
    bool ExtManager::requestLink(const Extension::LoadData& eld) {
        auto it = byID.find(eld.id);
        if(it == byID.end()) return false;
        else return this->requestLink(it->second, eld);
    }
    bool ExtManager::requestLink(ExtLibrary *lib, const Extension::LoadData& eld) {
        //std::cout << "attempting to link " << eld.id << "\n";
        return lib->link(eld);
    }
    
    void ExtManager::wasLinked(ExtLibrary *lib) {
        linked.insert(lib);
    }
    void ExtManager::wasUnlinked(ExtLibrary *lib) {
        linked.erase(lib);
    }
    
    void ExtManager::wasLoaded(ExtLibrary* lib) {
        if(lib == nullptr) return;
        //Super::wasLoaded(lib);
        //std::cout << lib->id()<< " was loaded\n";
        extensions.insert(lib);
        byID[lib->id()] = lib;
        
        if(!lib->type().empty()) byType[lib->type()].insert(lib);
    }
}
