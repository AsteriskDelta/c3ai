#include "ExtLibrary.h"
#include <ARKE/XML.h>
#include "ExtManager.h"
#include <dlfcn.h>

namespace C3AI {
    extern thread_local ExtManager *CurrentExtManager;
    
    ExtLibrary::ExtLibrary() : Loadable(), linked(false), manager_(CurrentExtManager) {
        
    }
    
    ExtLibrary::ExtLibrary(const std::string& p) : ExtLibrary() {
        this->load(p);
    }
    
    ExtLibrary::~ExtLibrary() {
        
    }
    
    bool ExtLibrary::couldLoad(const std::string& p) const {
        return p.size() >= 4 && p.find(".xml") != std::string::npos;
    }

    bool ExtLibrary::load(const std::string& p = "") {
        if(p.size() > 0) Loadable::path = p;
        if(!Loadable::load(Loadable::path)) {
            Console::Warn("ExtLibrary at ",Loadable::path," was requested, but not readable");
            return false;
        }
        
        XMLDoc doc;
        if(!doc.load(Loadable::path, false)) {
            Console::Warn("Extension Library XML file at ",Loadable::path," could not be parsed... Aborting.");
            return false;
        }
        
        xmlPath_ = Loadable::path;

        XMLNode root = doc.child("extension");
        if(!root) {
            Console::Warn("ExtLibrary at ",Loadable::path," has no root extension node");
            return false;
        }
        
        this->idPath_ = root.get<std::string>("path");
        this->idSub_ = root.get<std::string>("subID");
        this->type_ = root.get<std::string>("type");
        this->id_  = this->type_.empty() ? this->idPath_ : (this->idPath_ + "." + this->type_);
        this->id_ += this->idSub_.empty()? ""            : ("." + this->idSub_);
        this->name_ = root["name"].get<std::string>();
        
        //std::cout << "ID: " << this->id_ << "\n";
        
        for(XMLNode lib = root["library"]; bool(lib); lib = lib.sibling()) {
            auto pt = lib.get<std::string>("obj");
            if(pt.empty()) continue;
            
            auto pfx = lib.get<std::string>("pfx", "");
            //std::cout << "got prefix " << pfx << "\n";
            if(pfx.empty()) {
                //TODO: Error reporting
                Console::Out("\tExtLibrary ", this->id(), " failed to provide entry point prefix. Skipping...");
                continue;
            }
            
            bool abs = lib.get<bool>("abs", false);
            libs.push_back(LibData{pt, pfx, nullptr, abs});
            //std::cout << "\t" << libs.back().getFullPath(manager_) << "\t" << abs << "\n";
        }
        
        for(XMLNode lib = root["dep"]; lib; lib = lib.sibling()) {
            deps.push_back(lib.get<std::string>("id"));
        }
        
        for(XMLNode lib = root["conflict"]; lib; lib = lib.sibling()) {
            conflicts.push_back(lib.get<std::string>("id"));
        }
        
        return true;
    }
    
    bool ExtLibrary::isLoaded() const {
        return !this->id_.empty();
    }
    
    std::string ExtLibrary::getName() const {
        return this->id_;
    }
    
    void ExtLibrary::unload() {
        
    }
    
    ExtLibrary* ExtLibrary::Factory(const std::string p) {
        ExtLibrary* ret = new ExtLibrary();
        if(p.size() == 0) return ret;
        
        if(ret->load(p)) return ret;
        
        delete ret;
        return nullptr;
    }
    
    bool ExtLibrary::link(const Extension::LoadData& lData) {
        if(linked) return true;
        
        bool success = true;
        
        for(auto& lib : libs) {
            lib.handle = dlopen(lib.getFullPath(manager_).c_str(), RTLD_NOW | RTLD_GLOBAL);
            success &= lib.handle != nullptr;
            
            //std::cout << "\t\t\tdlopen got handle " << lib.handle << ", success="<<success<<"\n";
            if(!success) {
                Console::Err("Failed to load library at ", lib.getFullPath(manager_), " due to ", std::string(dlerror()),"\n");
                break;
            }
        }
        
        if(success) {
            for(auto& lib : libs) {
                void (*fn)(decltype(this), decltype(lData));
                const std::string entryPoint = lib.prefix + std::string("_OnLink");
                
                fn = reinterpret_cast<decltype(fn)>(dlsym(lib.handle, entryPoint.c_str()));
                
                if(fn != nullptr) (*fn)(this, lData);
                else {
                    Console::Err("Unable to locate entry point ",entryPoint," in ",lib.path);
                }
            }
        }
        
        return success;
    }
    
    bool ExtLibrary::unlink() {
        if(!linked) return true;
        
        for(auto& lib : libs) {
            void (*fn)();
            const std::string exitPoint = lib.prefix + std::string("_OnUnlink");
            
            fn = reinterpret_cast<decltype(fn)>(dlsym(lib.handle, exitPoint.c_str()));
            if(fn != nullptr) (*fn)();
            
            dlclose(lib.handle);
            lib.handle = nullptr;
        }
        linked = false;
        return true;
    }
    
    std::string ExtLibrary::getChildID(const std::string& i) const {
        if(i.empty()) return this->id_;
        else return this->id_ + std::string(".") + i;
    }
    
    std::string ExtLibrary::LibData::getFullPath(const ExtManager *const manager) const {
        if(absolutePath) return path;
        else return manager->extensionLibPath() + path;
    }
    
    
    void ExtLibrary::addExtension(Extension* ext) {
        extensions.insert(ext);
    }
    void ExtLibrary::removeExtension(Extension *ext) {
        extensions.erase(ext);
    }
}
