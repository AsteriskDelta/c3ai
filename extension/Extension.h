#ifndef INC_EXTENSION_H
#define INC_EXTENSION_H

namespace C3AI {
    class ExtLibrary;

    class Extension {
    public:
        struct LoadData {
            std::string id;
            std::unordered_map<std::string, std::string> args;
        };
        
        Extension(ExtLibrary *par, const LoadData& lData);
        virtual ~Extension();

    protected:
        ExtLibrary *parent_;
        LoadData loadData_;
    public:
        _getter(parent);
    };
};

#endif
