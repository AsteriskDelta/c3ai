#ifndef C3AI_INTRIN_LANG_H
#define C3AI_INTRIN_LANG_H

#ifndef CAI_EXTENSION_ID
#define CAI_EXTENSION_ID Intrin_Lang
#endif
#include "Intrinsic.inc.h"
#include "Char.h"

namespace C3AI {
    namespace Intrin {
        class Lang : public Extension {
        public:
            Lang(ExtLibrary *par, const Extension::LoadData& lData);
            ~Lang();
        protected:
            std::vector<Intrinsic*> chars;
            
            int loadAscii();
            int loadHirigana();
            int loadKatakana();
            int loadKanjiL1();
        };
    }
}

#endif

