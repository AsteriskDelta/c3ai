#ifndef INC_EXT_CHAR_H
#define INC_EXT_CHAR_H
#include "Intrinsic.inc.h"

typedef char16_t utf_t;

namespace C3AI {
    namespace Intrin {
        class Char : public Intrinsic {
        public:
            Char(utf_t c);
            virtual ~Char();
            
            virtual std::string toString() const;
        protected:
            utf_t rawChar;
        };
    };
};

#endif
