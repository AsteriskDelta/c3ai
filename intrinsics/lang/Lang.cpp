#include "Lang.h"
#include "AI.h"

namespace C3AI {
    namespace Intrin {
        Lang *LibLang = nullptr;
        Lang::Lang(ExtLibrary *par, const Extension::LoadData& lData) : Extension(par, lData) {
            std::stringstream ss; bool first = true;
            for(auto& lib : this->parent()->libs) {
                if(!first) ss << ", ";
                ss << lib.getFullPath(parent()->manager());
                if(first) first = false;
            }
            Console::$("Module", "\t", this->parent()->id(), " (",this->parent()->name(),") from ", ss.str());
            
            chars.resize(1024);
        }
        
        Lang::~Lang() {
            
        }
        
        int Lang::loadAscii() {
            int intrinsAdded = 0;
            for(utf_t i = 32; i < 128; i++) {
                Intrinsic *intrin = new Char(i);
                chars[i] = intrin;
                ActiveAI->registerIntrinsic(intrin);
                intrinsAdded++;
            }
            return intrinsAdded;
        }
    };
};

ON_EXTENSION_LINK() {
    using namespace C3AI::Intrin;
    LibLang = new Lang(par, lData);
    
}

ON_EXTENSION_UNLINK() {
    
}
