#include "Char.h"
#include <codecvt>

namespace C3AI {
    namespace Intrin {
        Char::Char(utf_t c) : Intrinsic() {
            rawChar = c;
        }
        Char::~Char() {
            
        }
        
        std::string Char::toString() const {
            char tmpChars[2] = {static_cast<char>(rawChar), 0x0};
            if(rawChar < 128) return std::string(tmpChars);
            else {
                std::wstring_convert<std::codecvt_utf8<utf_t>, utf_t> conv;
                return conv.to_bytes(
                    reinterpret_cast<utf_t>(rawChar)
                );
            }
        }
    };
};
