#include "IntrinTemplate.h"

namespace C3AI {
    namespace Intrin {
        IntrinTemplate  *LibTemplate = nullptr;
    }
}

ON_EXTENSION_LINK() {
    using namespace C3AI::Intrin;
    
    //LibTemplate->setParent(par);
    std::cout << "It works!\n";
    //std::cout << "Extensions ptr at " << C3AI::Extensions << "\n";
    
    //for(auto& lib : C3AI::Extensions->extensions) {
    //    std::cout << "\t" << lib->id() << "\t" << lib->name() << "\n";
    //}
}

ON_EXTENSION_UNLINK() {
    std::cout << "Template dyn lib says goodbye!\n";
}
