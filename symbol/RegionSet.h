#ifndef C3AI_REGION_SET_H
#define C3AI_REGION_SET_H
#include "Region.h"

namespace C3AI {
    
    class RegionSet {
    public:
        RegionSet();
        ~RegionSet();
        
        struct RegionData {
            RegionPtr ptr;
            struct Dir {
                Num probability;
            } implicates, explicates;
            inline operator RegionPtr&() { return ptr; }
            inline operator const RegionPtr&() const { return ptr; }
            inline RegionPtr operator->() const { return ptr; };
        };
        
        Region origin, range;
        
        std::vector<RegionData> regions;
        std::unordered_map<SymbolPtr, std::vector<RegionData*>> bySymbol;
    protected:
        
    };
}

#endif

