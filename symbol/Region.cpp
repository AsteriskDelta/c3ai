#include "Region.h"

namespace C3AI {
    Region::Region() : links() {
        
    }
    
    Region::Region(const Region& o) : links(o.links), totalWeight(o.totalWeight) {
        
    }
    
    Region::Region(const Region&& o) : links(std::move(o.links)), totalWeight(o.totalWeight) {
        
    }
    
    Region::~Region() {
        
    }
    
    void Region::update() {
        this->totalWeight = 0;
        for(Symlink& link : links) {
            this->totalWeight += link.weight;
        }
    }
    
    //Unweighted percent overlap
    Num Region::overlap(RegionPtr o) const {
        Num ret = 0.0;
        
        return ret;
    }
    //Weighted field overlap percent
    Num Region::score(RegionPtr o) const {
        Num ret = 1, oTotalWeight = 0;
        
        for(const Symlink& link : links) {
            Symlink *oLink = o->getLink(link.symbol);
            if(oLink == nullptr) continue;
            else oTotalWeight += oLink->weight;
            
            //We take the sqrt because otherwise a mid-low weight with a crazy low score wouldn't sufficiently effect the outcome'
            const Num relWeight = sqrt(link.weight / this->totalWeight);
            const Num linkScore = link.score(*oLink);
            
            ret *= pow(linkScore, relWeight);
        }
        
        //Account for all the dimensions we lack information on, but not too harshly
        ret *= sqrt(oTotalWeight / o->totalWeight);
        
        return ret;
    }
    
    void Region::push(RegionPtr offset) {
        
        
        offsets.push_back(offset);
    }
    void Region::pop() {
        RegionPtr offset = offsets.back();
        
        
        offsets.pop_back();
    }
    
    Symlink* Region::getLink(const Symbol *const sym) {
        for(auto& link : links) {
            if(link.symbol == sym) return &link;
        }
        
        return nullptr;
    }
}
