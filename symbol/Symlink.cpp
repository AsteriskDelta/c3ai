#include "Symlink.h"
#include <NVX/NGaussian.impl.h>
#include <sstream>


namespace nvx {
    template class Gaussian<::C3AI::Num>;
};

namespace C3AI {
    Symlink::Symlink() : Distrib(), symbol(nullptr), weight(0) {
        
    }
    
    Symlink::Symlink(const Symlink& o) : Distrib(o), symbol(o.symbol), weight(o.weight), cache(o.cache) {
        
    };
    
    Symlink::Symlink(SymbolPtr t) : Distrib(), symbol(t) {
        
    }
    
    void Symlink::update() {
        Num range = this->variance() == Num(0)? Num(0) : Num(this->invpdf(Num(1) - Epsilon));
        cache.min = this->mean() - range;
        cache.max = this->mean() + range;
    }
    
    Num Symlink::mean(Num newMean) {
        this->setMean(newMean);
        this->update();
        return this->mean();
    }
    
    //Should we even bother checking?
    bool Symlink::intersects(const Symlink& o) const {
        return  this->symbol == o.symbol && (this->mean() == o.mean() ||
                (
                    (this->min() < o.max() && this->max() > o.min()) ||
                    (this->max() < o.max() && this->min() > o.min())
                ));
    }
    
    bool Symlink::encloses(const Symlink& o) const {
        //std::cout << "Enclose check: " << this->min() << " <= " << o.min() << "? " << this->max() << " >= " << o.max() << "? = " << bool(this->symbol == o.symbol && this->min() <= o.min() &&  this->max() >= o.max()) << "\n";;
        return this->symbol == o.symbol && 
            this->min() <= o.min() && 
            this->max() >= o.max();
    }
    
    //Integral of probability shared with other distribution
    Num Symlink::intersection(const Symlink& o) const {
        return this->intersect(o);
    }
    Num Symlink::intersection2(const Symlink& o) const {
        return sqrt(this->intersection(o));
    }
    
    Num Symlink::intersectionPoint(const Symlink& o) const {
        return Distrib::Vertex(this, &o);
    }
    
    Num Symlink::intersectionProb(const Symlink& o) const {
        return this->pdf(this->intersectionPoint(o));
    }
    
    //Probability (0-1) of expected value being contained by the other distribution
    Num Symlink::contains(const Symlink& o) const {
        return this->pdf(o.mean());
    }
    Num Symlink::contained(const Symlink& o) const {
        return o.pdf(this->mean());
    }
    
    Num Symlink::affinity(const Symlink& o) const {
        return sqrt(this->contains(o) * this->contained(o));
    }
    
    Num Symlink::score_(const Symlink&o) const {
        //const Num prb = this->intersect(o);//Integral from CDFs
        //std::cout << "score_ got prb=" << prb << "\n";
        
        const Self *ag, *bg;
        bool toggle = false;
        
        if(this->mean() <= o.mean()) {
            ag = this; bg = &o;
        } else {
            ag = &o; bg = this;
            toggle = true;
        }
        const Num cp = Vertex(ag, bg);
        Num prob;
            
        const Symlink *eg = nullptr;
        if(ag->encloses(*bg)) eg = bg;
        else if(bg->encloses(*ag)) eg = ag;
        
        //Special case for dirac
        if(this->variance() == o.variance() && this->variance() == 0) prob = Num(1.0);
        else if(this->variance() == 0) prob = o.pdf(cp);
        else if(o.variance() == 0) prob = this->pdf(cp);
        else if(eg != nullptr) {//Special case for enclosed distributions
            const Num diff = (cp - eg->mean());
            const Num otherVertex = eg->mean() - diff;

            //std::cout << "double intersection at " << cp << " and " << otherVertex << "\n";
            
            if(eg == ag) prob = bg->cdf(nvx::max(cp, otherVertex)) - bg->cdf(nvx::min(cp, otherVertex));
            else prob = ag->cdf(nvx::max(cp, otherVertex)) - ag->cdf(nvx::min(cp,otherVertex));
        } else {// Two normal, intersecting distributions
            Num deltaA = cp - ag->mean(),
                deltaB = bg->mean() - cp;
            _unused(deltaA)
            prob = bg->cdf(cp) + 
                    (ag->cdf(cp+deltaB) - ag->cdf(cp));
        }
        //std::cout << "score_ got " << cp << " for ap=" << aProb << ", bp=" << bProb << "\n";
        //They represent probability where ap + bp = 1, so multiply both by two for their own relative values
        
        return prob;
        //aProb *= Num(2);
        //bProb *= Num(2);
        
        
        
        //std::cout << "\taprb=" << aProb << ", bprb=" << bProb << "\n";
        
        //if(toggle) return ScoreData{bProb/* * bg->weight*/, aProb/* * ag->weight*/};
        //else return ScoreData{aProb/* * ag->weight*/, bProb/* * bg->weight*/};
    }
    
    Num Symlink::score(const Symlink& o) const {
        auto dat = this->score_(o);
        return dat;
    }
    /*
    Num Symlink::mutualScore(const Symlink& o) const {
        auto dat = this->score_(o);
        return sqrt(dat.a*dat.b);
    }
    //Weighted score of peak distribution intersection
    Num Symlink::symmetricScore(const Symlink &o) const {
        auto dat = this->score_(o);
                    
        return dat.a;//(dat.a * dat.b);// / (this->weight * o.weight);
    }
    Num Symlink::symmetricAvgScore(const Symlink &o) const {
        auto dat = this->score_(o);
        return (dat.a+ dat.b) / Num(2);
    }
    Num Symlink::asymmetricScore(const Symlink &o) const {
        auto dat = this->score_(o);
        return (dat.a);// / (this->weight) * (this->weight/o.weight);
    }
    
    Num Symlink::differential(const Symlink& o) const {
        //auto dat = this->score_(o);
        return this->asymmetricScore(o) / o.asymmetricScore(*this);//-(dat.a - dat.b) * (dat.a * dat.b);
    }*/
    
    std::string Symlink::debugInfo() const {
        std::stringstream ss;
        ss << "Symlink{M:" << this->mean() << " S:" << this->stddev() << " V:" << this->variance() << " (" << this->min() << "-" << this->max() << ") W:" << this->weight << " 0x" << symbol << "}";
        return ss.str();
    }
}
