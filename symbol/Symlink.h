#ifndef C3AI_SYMLINK_H
#define C3AI_SYMLINK_H
#include "C3Include.h"
#include <NVX/NGaussian.h>

namespace C3AI {
    class Symbol;
    
    //Attributes: symbol, weight, mean, variance
    class Symlink : public Gaussian<Num> {
    public:
        typedef Gaussian<Num> Distrib;
        typedef Symlink Self;
        
        Symlink();
        Symlink(const Symlink& o);
        Symlink(SymbolPtr t);
        
        //Build the cache;
        void update();
        using Distrib::mean;
        Num mean(Num newMean);
        
        //Should we even bother checking?
        bool intersects(const Symlink& o) const;
        //Is o entirely contained within us?
        bool encloses(const Symlink& o) const ; 
        
        //Integral of probability shared with other distribution
        Num intersection(const Symlink& o) const;
        Num intersection2(const Symlink& o) const;
        
        Num score(const Symlink& o) const;
        //Num mutualScore(const Symlink& o) const;
        
        Num intersectionPoint(const Symlink& o) const;
        Num intersectionProb(const Symlink& o) const;
        
        //Probability (0-1) of expected value being contained by the other distribution
        Num contains(const Symlink& o) const;
        Num contained(const Symlink& o) const;
        Num affinity(const Symlink& o) const;
        
        //Weighted score of peak distribution intersection
        //Num symmetricScore(const Symlink &o) const;
        //Num symmetricAvgScore(const Symlink &o) const;
        //Num asymmetricScore(const Symlink &o) const;
        
        //Returns negative if o > this, otherwise positive
        //Num differential(const Symlink &o) const;

        SymbolPtr symbol;
        Num weight;
        
        inline operator bool() const {
            return symbol != nullptr;
        }
        
        inline operator<(const Symlink& o) const {
            return this->cache.max < o.cache.min;
        }
        inline operator>(const Symlink& o) const {
            return this->cache.min > o.cache.max;
        }
        inline const Num& min() const {
            return cache.min;
        }
        inline const Num& max() const {
            return cache.max;
        }
        
        std::string debugInfo() const;
    protected:
        constexpr const static Num Epsilon = Num(0.001);
        
        struct ScoreData {
            Num a, b;
        };
        Num score_(const Symlink&o) const;
        
        struct Cache {
            Num min, max;
        } cache;
    };
}

#endif
