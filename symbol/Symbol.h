#ifndef C3AI_SYMBOL_H
#define C3AI_SYMBOL_H
#include "RegionSet.h"

namespace C3AI {
    class Intrinsic;
    extern ptr_t IntrinsicSize;
    
    class Symbol : public RegionSet {
    public:
        Symbol();
        Symbol(const RegionSet& rs);
        ~Symbol();
        
        inline Intrinsic* intrinsic() {
            if(!flags.intrinsic) return nullptr;
            else {
                 ptr_t ptr = reinterpret_cast<ptr_t>(this);
                 ptr = ptr - IntrinsicSize;
                 return reinterpret_cast<Intrinsic*>(ptr);
            }
        }
        
        struct Flags {
            bool intrinsic : 1 = false;
        } flags;
    protected:
        
    };
}

#endif
