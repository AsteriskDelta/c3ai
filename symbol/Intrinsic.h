#ifndef C3AI_INTRIN_H
#define C3AI_INTRIN_H
#include "C3Include.h"
#include "Symbol.h"
#include <ARKE/Identified.h>

namespace C3AI {
    
    class Intrinsic : public Symbol,
    public Named, public Identified<Intrinsic*> {
    public:
        typedef Identified<Intrinsic*> Identified_t;
        
        Intrinsic();
        Intrinsic(const std::string& i, const std::string& n  = "");
        virtual ~Intrinsic();
        
        inline bool isOperation() {
            return data.handler != nullptr;
        }
        
        virtual std::string toString() const;
    protected:
        struct Data {
            void (*handler)(Symbol*) = nullptr;
            ExtLibrary *owner = nullptr;
            Interface *interface = nullptr;
        } data;
    };
    
    namespace Intrin {
        
    };
};

#endif
