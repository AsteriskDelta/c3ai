#ifndef C3AI_REGION_H
#define C3AI_REGION_H
#include "C3Include.h"
#include "Symlink.h"
#include <vector>

namespace C3AI {
    class Region {
    public:
        Region();
        Region(const Region& o);
        Region(const Region&& o);
        ~Region();
        
        inline Region& operator=(const Region&o) {
            Region *t = this;
            t->~Region();//self-destruct
            return *(new(t) Region(o)); // reconstruct
        }
        inline Region& operator=(const Region&& o) {
            return (*this = std::move(o));
        }
        
        void update();
        
        Symlink* getLink(const Symbol *const sym);
        inline Symlink* operator[](const Symbol *const sym) {
            return this->getLink(sym);
        }
        
        //Number of symlinks * strictness of their distribution
        Num complexity() const;
        
        //Unweighted percent overlap
        Num overlap(RegionPtr o) const;
        inline Num overlap(RegionPtr o, RegionPtr pointOfRef) {
            this->push(pointOfRef);
            const Num ret = this->overlap(o);
            this->pop();
            return ret;
        }
        //Weighted field overlap percent
        Num score(RegionPtr o) const;
        Num score(RegionPtr o, RegionPtr pointOfRef) {
            this->push(pointOfRef);
            const Num ret = this->score(o);
            this->pop();
            return ret;
        }
        
        //Num evaluate(RegionPtr center = nullptr) const;
    protected:
        std::vector<Symlink> links;
        Num totalWeight;
        
        std::list<RegionPtr> offsets;
        
        void push(RegionPtr rel);
        void pop();
    };
}

#endif
